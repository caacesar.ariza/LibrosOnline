import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonPageEvent } from 'src/app/models/common-page-event';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.sass']
})
export class PaginatorComponent implements OnInit, CommonPageEvent {
  @Input() length!: number;
  @Input() pageZise!: number;
  @Input() pageIndex: number = 1;
  @Input() lengthPage!: number;
  @Input() pageOptionZise!: number[];
  @Output() changePage: EventEmitter<CommonPageEvent> = new EventEmitter<CommonPageEvent>();


  ngOnInit(): void {
    this.getLengthPages();
  }

  next(): void {
    console.log("contador", this.pageIndex)

    this.pageIndex < this.lengthPage ? this.pageIndex++ : this.pageIndex;
    this.changePage.emit(this);
  }
  prev(): void {
     this.pageIndex > 1 ? --this.pageIndex : this.pageIndex;
    this.changePage.emit(this);
  }
  getCurrentPage(page: number): void {
    this.pageIndex = page;
    this.changePage.emit(this);
  }
  getLengthPages(): void {
    this.lengthPage = Math.ceil(this.length / this.pageZise);
    console.log("contador", this.pageIndex)

  }

  calculateTotalPages(_length: number, _pageZise: number): Array<number> {
    let element: Array<number> = [];
    this.length = _length;
    this.pageZise = _pageZise
    for (let index = 1; index <= this.lengthPage; index++) {
      element.push(index);
    }
    return element
  }

}
