import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild, asNativeElements } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { Modal } from '../share/modals';
import { DOCUMENT } from '@angular/common';
import { WindowRef } from '../share/WindowRef';
import { Observable, debounceTime, filter, fromEvent, map, switchMap } from 'rxjs';
import { CommonPageEvent } from 'src/app/models/common-page-event';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  // @ViewChild('asComponente', { static: true }) com !: ElementRef<HTMLDivElement>;
  //@ViewChild('asModal', { static: true, read: ElementRef }) modal !: ElementRef<any>;
  modales: any
  arraya: Array<any> = []
  b: string = "buneas"
  @ViewChild('input', { static: true, read: ElementRef }) input !: ElementRef<any>;
  ipunt$!: Observable<any>;

  /**
   * paginacion example
   */
  length: number = 40;
  pageZise: number = 15;
  pageIndex: number = 1;
  pageOptionZise: number[] = [5, 10, 15, 20, 30, 100]


  constructor(
    private render: Renderer2,
    private win: WindowRef
  ) {
    console.log('constructor')
  }

  ngOnInit(): void {
    console.log('OnInit')
    //console.log(this.modal.nativeElement.id)
    //console.log(document.getElementById("exampleModal"))
    // this.modales=  this.win.boostrapModal("exampleModal");
    //this.modales.show()
    /**
     * win.bootstrap.Modal(this.modal.nativeElement,{
   keyboard: false
 })
     */
    fromEvent<Event>(this.input.nativeElement, "change")
      .pipe(

        map((event: Event) => {
          const fecha = (event.target as HTMLInputElement).value
          console.log(fecha)
          return fecha
        }),
        //filter((search: string)=> { console.log(search)}),
        debounceTime(200),
        //switchMap((searh:string)=> console.log(search)),
      )
      .subscribe(data => {
        console.log(data)
        return data
      })
    console.log("input $", this.ipunt$)
  }

  clics(): void {
    //console.log(this.com.nativeElement);
    //this.render.addClass(this.com.nativeElement, "prueba")
  }
  prueba(): void {
    //this.modales.show()
  }
  changePage(e: CommonPageEvent): void {
    this.pageIndex=e.pageIndex;
    console.log("cambia", this.pageIndex);

  }

}
