import { Inject, Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})

export class WindowRef {
    win: any = window
    private _modalShow: any 
    
    constructor() { 
        console.log(this.win)
    }
    public get modalShow(): any {
        return this._modalShow;
    }
    public set modalShow(value: any) {
        this._modalShow = value;
    }

    boostrapModal(idModal: string): void {
        return new this.win.bootstrap.Modal(document.getElementById(idModal), {
            keyboard: false
        })
    }
}