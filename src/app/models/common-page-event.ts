export interface CommonPageEvent {
    /**
     * cantidad de registros
     */
    length: number,
    pageZise: number,//cantidad por pagina
    pageIndex: number,//pagina que se muestra
    lengthPage: number,//cantidad  de paginas
    pageOptionZise:Array<number>
    /*
    metodos por pagina implements 
     */

    next():void
    prev():void
    getCurrentPage(page: number):void
    getLengthPages():void
}

